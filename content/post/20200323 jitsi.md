---
title: From Zero to Jitsi Meet in 30 Minutes
date: 2020-03-23
tags:
- Ansible
- Jitsi Meet
- Let's Encrypt
- Ubuntu
---

The following guide will show you how you deploy your own [_Jitsi Meet_][jitsi-meet] server in no-time on your own virtual server.

<!--more-->

[jitsi-meet]: https://jitsi.org/jitsi-meet/

## Overview

There are three basic steps to get to your own Jitsi Meet server:

- Install Ansible
- Get an Ubuntu 18.04 virtual server
- Run the Ansible playbook on that server

## Disclaimer on the 30 minutes

The thirty minutes claim will likely not hold up if

- you're on Windows and haven't set up and gotten used to WSL yet
- you're on macOS and haven't set up [Homebrew][brew] yet
- you don't have an account with [Free DNS][freedns] already
- you don't have an account with [Vultr][vultr] or any other cloud service provider already
- it takes [Free DNS][freedns] more than the usual couple of minutes to propagate your domain name

It will take you only about 15 minutes if you already know how a terminal, cloud service providers, DNS, SSH and Ansible work.
But I would suggest that you head over to [the `Quick Start` section of the respective Ansible role _ansible-jitsi-meet-role_][cimnine-jitsi-meet] instead.

[cimnine-jitsi-meet]: https://github.com/cimnine/ansible-jitsi-meet-role/#quickstart

## Install Ansible

Ansible is a tool to automate server configuration.
We will use Ansible later to install and configure Jitsi Meet on your server.
But first, you will need to [install Ansible on your own computer](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html).

**macOS** users that don't have Homebrew installed yet see the sub-section _Install Ansible on macOS_ below.\
**Windows** users see the sub-section _Install Ansible on Windows_ below as well.

All other users should find the following commands good enough to install Ansible on their system. Otherwise, [refer to the original _Installing Ansible_ guide][ansible-install].

```bash
# on macOS with Homebrew
brew install ansible

# on Debian or Ubuntu
sudo apt update
sudo apt install software-properties-common
sudo apt-add-repository --yes --update ppa:ansible/ansible
sudo apt install ansible

# or via Python / pip
sudo python3 -m pip install ansible
```

[ansible-install]: https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html

### Install Ansible on macOS

If you haven't installed [Homebrew][brew] yet, it is time for you to do so.
First open the _Terminal_ app on your macOS.
It's under `/Applications/Utilities/Terminal.app`.

First, enter the following command in the Terminal.
You may _copy and paste_ it, of course.
To confirm the command, hit the enter key.

```bash
xcode-select --install
```

Make sure that the installation of the _Command Line Tools (CLT) for Xcode_ completes before you continue.

Now use your web browser (Safari, Firefox, ...) to navigate to [_brew.sh_][brew].
You will see the _Install Homebrew_ headline and below that a line which starts with `/bin/bash -c`.
Copy the whole line to your clipboard, switch to your Terminal and paste it.
Then hit the enter key.

The installer will tell you what it's about to do and will ask you questions.
If you don't know better, then the default answers will be fine for you.

After Homebrew is installed run the following command in your Terminal to install Ansible:

```bash
brew install ansible
```

As soon as Homebrew completed the installation, try to run `ansible --version` to check whether the installation was successful.
You should see something like `ansible 2.9.1` and some more information printed back to you.

[brew]: https://brew.sh/

### Install Ansible on Windows

If you are running a recent version of Windows 10, you can install the _Windows Subsystem for Linux 2_ (WSL).
It will provide you with a Linux installation that integrates into your Windows 10.

[Follow this first guide on how to install WSL][win10] and [then this second guide on how to initialize WSL.][win-distro]
**Make sure to install the [Ubuntu 18.04 LTS][win-ubuntu] distribution from the Windows Store!**

Once Ubuntu runs on your Windows 10, you can use the following commands to install Ansible:

```bash
sudo apt update
sudo apt install software-properties-common
sudo apt-add-repository --yes --update ppa:ansible/ansible
sudo apt install ansible
```

Run `ansible --version` to check whether the installation was successful.
You should see something like `ansible 2.9.1` and some more information printed back to you.

[win10]: https://docs.microsoft.com/en-us/windows/wsl/install-win10
[win-distro]: https://docs.microsoft.com/en-us/windows/wsl/initialize-distro
[win-ubuntu]: https://www.microsoft.com/store/apps/9N9TNGVNDL3Q

## Virtual Server

In this guide we are going to deploy a virtual server on [Vultr][vultr].
Vultr is a US-based cloud service provider that is easy to use and that has data centers all over the world to choose from.
You may also deploy a server on any other cloud service provider, like [Digital Ocean](https://m.do.co/c/50be0310e60b), [Hetzner Cloud](https://www.hetzner.com/cloud), [Cloudscale](https://www.cloudscale.ch/), Azure, Google Cloud, AWS, ...

If you don't have an account with Vultr already, you will need to create one first.
You can use my [referral link][vultr] to get a $100 credit, valid for the first 30 days.
(The offer was valid at the time of writing and may have expired by now.)

After you logged in, open the _Products_ section on the left and hit the blue `+` button on the right [(or use this direct link)](vultr), then follow these steps:

1. Make sure that you create a new _Cloud Compute_ instance, which is just Vultr's fancy name for a virtual machine.
1. Choose your favorite server location.
1. Choose an _Ubuntu 18.04 x64_ server.
1. Choose the server size you like.
   (You must choose at least the _$5/mo_ size.)
1. Select or add your SSH _public key_.
   If you don't know how this works, [follow Vultr's guide][vultr-ssh-key].
   (**All users should follow the Linux instructions of that guide.**
   I.e. if you are on macOS, follow the Linux instructions.
   If you are on Windows, make sure to run the Linux instructions in your WSL system.)
1. Ensure that _No Firewall_ is selected as _Firewall Group_.
1. Enter a _Server Hostname_ and a _Server Label_, e.g. `jitsi-meet`.

Then click _Deploy Now_.
Your server will be ready within minutes.
Once it is ready you will get an email.
Then it's time to open the server's detail page.
(You might need to refresh the products overview page.)

Copy (or write-down) the _IP Address_ of your server.
The _IP Address_ consists of four numbers in the format of `123.123.123.123`.

[ubuntu1804]: http://releases.ubuntu.com/18.04/
[vultr]: https://www.vultr.com/?ref=8496145-6G
[vultr-deploy]: https://my.vultr.com/deploy/
[vultr-ssh-key]: https://www.vultr.com/docs/how-do-i-generate-ssh-keys

## DNS

DNS is what gives your server a name.
DNS stands for _domain name system_.
`brainfood.xyz` is the domain name of this website.
Since computers actually communicate via IP, any computer since the 90s knows how to get the corresponding IP address from a domain name.

So, for DNS we'll use the [Free DNS service of afraid.org][freedns].
First, [create an account with them if you don't have one already][freedns-signup].
Make sure to activate your account via the email they're sending you.

After you've logged in, [create a new _Subdomain_][freedns-subdomain].
Provide the following values:

- _Type_: `A`
- _Subdomain_:
  Choose a name.
  This will be one part of your server's domain name.
- _Domain_:
  Select a domain you like.
  Together with the subdomain this will form the full domain name of your server, i.e.
  `subdomain.domain.com`.
- Destination:
  Paste the _IP Address_ of your server, which you've copied (or have written-down) from the server's details page on Vultr.

Solve the captcha and click "_Save!_".

[freedns]: https://freedns.afraid.org/
[freedns-signup]: https://freedns.afraid.org/signup/
[freedns-subdomain]: https://freedns.afraid.org/subdomain/edit.php

## SSH

Since the server running somewhere in one of Vultr's data centers we have to have a way of connecting to that server and install stuff on it.
That's where SSH comes into play.
It allows us to open a secure and encrypted channel to our server.

Ansible will later connect to your server to install the Jitsi Meet software.
That's why you need to make sure that you can connect to your server.
Run the following command.
Make sure to substitute `subdomain.domain.com` with the actual subdomain and domain name that you've created at _Free DNS_.

```bash
ssh root@subdomain.domain.com
```

> If you get an error message that reads `Could not resolve hostname subdomain.domain.com`, you can replace `subdomain.domain.com` with the _IP address_ of your server in the command above.

You should not get asked for a password.
If you do, then you need to fix this first.

Go to the Vultr page with your server's details.
There you will be able to copy the _root_ user's password to your clipboard.
Then run the following commands to copy your SSH public key from your computer to your new server.
(Remember to replace `subdomain.domain.com` with your server's actual subdomain and domain name.)

> If you're on macOS, run the following command first: `brew install ssh-copy-id`

```bash
ssh-copy-id root@subdomain.domain.com
ssh root@subdomain.domain.com
```

## Prepare an Ansible Inventory

Ansible works using a so-called _Inventories_.
These are files that contain a list of all the servers which should be managed by Ansible.

Create a new directory somewhere on your computer and call it `jitsi-meet`.
(And remember that folder's location or write it down.)
Then, in that folder, create a new file called `jitsi.ini`.
The file should contain the following content.

> Make sure that you replace the following values with the actual values:
>
> - `subdomain.domain.com`: Domain name of your server, which you've just created
> - `your.actual@email.com`: A working email address of you

```ini
[jitsi]
subdomain.domain.com

[jitsi:vars]
certbot_admin_email=your.actual@email.com
ansible_user=root
apt_mirror=http://archive.ubuntu.com/ubuntu
certbot_enabled=yes
```

Save the file.

## Prepare an Ansible Playbook

In Ansible, a _Playbook_ is a set of instructions and configurations which Ansible will apply to your servers.
We'll one instruction that tells Ansible to install _Jitsi Meet_ on the server which you've created.

Now create a new file in that same `jitsi-meet` directory from before, but call it `jitsi.yml` this time.
Add the following content.

> Note: \
> Make sure, that there are two spaces to the left of `roles:`, \
> and four spaces to the left of `- jitsi`.

```yaml
- hosts: jitsi
  roles:
    - cimnine.jitsi_meet
```

Save the file.

## Fetch the Required Ansible Dependencies

Just out of the box Ansible does not know how to install _Jitsi Meet_ onto a server.
That's why we're going to install a so-called _Role_.
A _Role_ is an instruction-set that tells Ansible how to do something.
In this case it tells Ansible how to install _Jitsi Meet_ on a server running _Ubuntu 18.04_.

Execute the following two commands in your terminal.
They will install the _jitsi-meet_ role and the _certbot_ role to your system.
(The latter is required by _jitsi-meet_ and makes sure that your future video conference participants will communicate over a secure connection with your server.)

```bash
ansible-galaxy install cimnine.jitsi_meet
ansible-galaxy install geerlingguy.certbot
```

## Run the playbook

Now that we have an Ansible _Inventory_ which contains our server and an Ansible _Playbook_ that tells Ansible to install _Jitsi Meet_ it is time to execute the plan.

The following command tells Ansible to run the task defined in the playbook `jitsi.yml` on the server that is defined in the inventory file called `jitsi.ini`.

In order for the command to work, you must first change into the `jitsi-meet` directory.
For example when you're on macOS and that directory is in `~/Documents/jitsi-meet/`, then you will need to run `cd ~/Documents/jitsi-meet/` in your terminal. In a terminal you can change directories with the `cd` command. See the sub-section _Home Directories_ below about the tilde (`~`) and  home-directories in general.

> You can run `ls` to see if your `jitsi.ini` and `jitsi.yml` files are in the current directory.
> If they're not, you are likely in the wrong directory.
> To see in which directory you currently are, run `pwd`.

When you've successfully changed the directory to the `jitsi-meet` directory, then run the final command:

```bash
ansible-playbook -i jitsi.ini jitsi.yml
```

It will connect to your server and install _Jitsi Meet_ and configure everything.

After the command ran successfully, you should have your own Jitsi Meet server ready at `https://subdomain.domain.com` 🚀.

### Error _Could not resolve hostname_

If you get an error message that reads `Could not resolve hostname subdomain.domain.com`, then change the inventory file `jitsi.ini` slightly.
Add the `ansible_host` variable after your host and replace `123.123.123.123` with the actual IP Address of your server:

```ini
[jitsi]
subdomain.domain.com ansible_host=123.123.123.123

[jitsi:vars]
certbot_admin_email=your.actual@email.com
ansible_user=root
apt_mirror=http://archive.ubuntu.com/ubuntu
certbot_enabled=yes
```

### Home Directories

On Linux, macOS and other BSD-based systems, the tilde character, i.e. `~`, usually refers to your home directory.

Your home directory on macOS usually is `/Users/your_username/` and on Linux and BSD-based systems it's usually `/home/your_username`.

On Windows it's different.
There your home directory is usually `%HOMEPATH%`.
But when you're in _Windows Subsystem for Linux (WSL)_, then your home directory will be at `/mnt/c/Users/your_username/`.
