---
title: Host Your Own Blog with Gitlab and Netlify
date: 2019-05-18
tags:
- Netlify
- Gitlab
- Hugo
- Let's Encrypt
---

This guide explains how to host a own blog / website for free with about 1 hour of effort.
It's how I host this blog.

There are certainly other options, and I've used some in the past.
But this worked very well and with very little configuration effort.

In this guide I use [Gitlab][gitlab] as code host and [Netlify][netlify] to publish the website.
I'm using the [Hugo][hugo] build system to build a static page.

<!--more-->

[Gitlab]: https://gitlab.com
[Netlify]: https://netlify.com
[hugo]: https://gohugo.io

# Result

You'll get a website / blog, that is automatically deployed whenever you push changes to your Git repository.
The site will have free TLS (aka "https") and IPv6.
If you're willing to spend about USD 10 a year you will also get your own domain name.

# Preconditions

I'm assuming you already have a Gitlab account (it's free) and that you're logged-in to Gitlab.

# Setting up Gitlab

Go to https://gitlab.com/pages/nfhugo and fork the repository.

In your cloned repository, click _Settings_ on the left.
Change your _Project Name_ and click _Save changes_.

Then scroll down to _Advanced_ and click _Remove the Forking Relationship_.

Afterwards, once more open the _Advanced_ section and change the _Path_ to something that correlates with your project name, e.g. "blog".

# Setting up Netlify

If this is the first time you're using Netlify, go straight ahead to their [_Sign Up_ page][netlify_signup].
I recommend to register with an email address, but you might as well use you Gitlab account right away.

Once you've completed the signup process, you should add a _New site from Git_.
Choose your Git provider.
If you're following this guide, then that would be _Gitlab_.

Select your repository in the next step, e.g. "blog".
At the third step just click _Deploy Site_.

You're website will now be deployed by Netlify and you will see the URL that Netlify generated for you.
Click it, and you should see your blog / website.

[netlify_signup]: https://app.netlify.com/signup

# Changing Content

There are two ways to edit the content of your website.
The easiest is using the Web IDE of Gitlab.
Or you can also edit your pages on your local machine.

## Web IDE

Go to the _Repsitory_ page of your Gitlab project.
In the upper-right section you should see the _Web IDE_ button.
Press it, and it will take you straight to an online editor.

## Local Editing

_Hint: I'm assuming you know how Git works_

You can clone the repository to your computer and edit the files locally.
To preview the changes, you can install [Hugo][hugo] to your computer.

Run `hugo serve` from the project root directory.
It will echo a URL on which you will find a preview of your page.

## File Structure

Your content pages are in the `content` directory.
The `page` directory contains general pages, such as the "about" page.
The `post` directory contains your blog posts.
You should be able to adjust the pages by looking at the examples present.

There is one more important file, and it's the `config.toml` file.
It contains some basic information about your blog which you should adjust.
If you're interested in how it could look like, have a look at my [`config.toml`][brainfood-config] file.

Head over to [the Hugo website][hugo] for more information about the file structure, all the settings and how it all works together.
It explains anything in great detail.

[brainfood-config]: https://gitlab.com/cimnine/brainfood.xyz/blob/4c092f412de85d97b1a4dc92ed830ca5f2eff0f0/config.toml

## Advanced Topic: Branch Preview

By default, if you push a _branch_ to Gitlab, Netfify will create a preview deployment.
You can find the URL to these on your site's Netlify Dashboard under _Deploy Previews_.

It's a great way to preview a branch to someone else.

# Bonus: Custom Domain

It's very easy to add a custom domain to your site, but domains are not free.
But don't worry, they're mostly not expensive either.

You can get domains such as `something.xyz` for USD 10 per year.

## Domain Registration

The first step is to register a domain somewhere.
My domain is registered via [gen.xyz][genxyz].

If you choose to register with [gen.xyz][genxyz], pay attention to the following:

* Select _1 year_ term.
* Make sure to opt-out of any of the promotional offers they have (unless you want them).
* Enter the promo code `GENXYZ` and you will get the first year for just USD 1.

After you paid via credit card or PayPal you will get lots of emails.
**There will be one important email** where you will need to confirm you email-address.
It will usually arrive quite a bit after all the other emails arrive, and you can't use the domain until you've confirmed your email-address.

[genxyz]: https://gen.xyz/a/16952

### Register via Netlify

You can also register your domain via Netlify.
This is usally a bit more expensive, but it will be way easier in terms of configuration.
If you want to do that, then just follow the steps below.
But instead of a domain you already own, you'd enter the domain you'd like to have.

## Configure Netlify

Now that you own a domain, go to your Netlify account and click on _Domains_.
Choose _Add or Register a Domain_ and enter your newly registered domain.

When it asks you to add DNS records, you may gladly skip over it.
(i.e. click _Continue_)

In step three it will show you four nameservers.
They will look similar to these:

* dns1.p04.nsone.net
* dns2.p04.nsone.net
* dns3.p04.nsone.net
* dns4.p04.nsone.net

Note them down, as they are required in the next step.

Then click _Done_.

Now, on the top right, you should see a _Enable IPv6_ button.
I recommend to click it, as it might give your site an extra speed boost in certain situations.

## Configure Nameservers

You will now have to connect your domain with Netlify.
If you registered with gen.xyz, navigate to your [account dashboard][genxyz_account].
Click the grey _Manage_ button of your domain.

In the section where it says _Nameserver_, make sure the option _Use custom nameservers_ is active.
Then enter the four nameservers you have noted down before. Click _Save Nameservers_.

It will usually take a while until your domain is reachable accross the internet.
(A few hours is very common.)

Netlify will periodically check whether the domain points to it.
As soon as the change has probagated through the internet, it will automatically create a [Let's Encrypt][letsencrypt] certificate for you, so that your blog post is secured via TLS (i.e. `https`).

[genxyz_account]: https://gen.xyz/account/clientarea.php
[letsencrypt]: https://letsencrypt.org

# Congratulations

You have sucessfully deployed a personal website / blog.
It is secured via TLS (`https`) and reachable via IPv6.
Now make sure that you keep it up-to-date and that anyone knows about it.
